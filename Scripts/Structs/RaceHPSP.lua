-- Race HP/SP modifiers
local Id = "RaceHPSP"
local Log = Log
Log(Merge.Log.Info, "Init started: %s", Id)

local max = math.max
local asmpatch, autohook, hook = mem.asmpatch, mem.autohook, mem.hook

local function ProcessRaceHPSPTxt()
	local RaceHPSP = {}

	local TxtTable = io.open("Data/Tables/RaceHPSP.txt", "r")

	if not TxtTable then
		Log(Merge.Log.Warning, "No RaceHPSP.txt found, creating one.")
		TxtTable = io.open("Data/Tables/RaceHPSP.txt", "w")
		TxtTable:write("#\9Note\9HPBase\9HPFactor\9SPBase\9SPFactor\n")
	else
		local LineIt = TxtTable:lines()
		local header = LineIt()
		if header ~= "#\9Note\9HPBase\9HPFactor\9SPBase\9SPFactor" then
			Log(Merge.Log.Error, "RaceHPSP.txt header differs from expected one, table is ignored. Regenerate or fix it.")
		else
			local linenum = 1
			for line in LineIt do
				linenum = linenum + 1
				local Words = string.split(line, "\9")
				if string.len(Words[1]) == 0 then
					Log(Merge.Log.Warning, "RaceHPSP.txt line %d first field is empty. Skipping following lines.", linenum)
					break
				end
				-- Ignore lines that don't start with number
				if Words[1] and tonumber(Words[1]) then
					local hp_sp_mods = {}
					local race_id = tonumber(Words[1])
					-- Skip Words[2] since it contains Race StringId
					hp_sp_mods.HPBase = tonumber(Words[3])
					-- Skip divisor if HPFactor is written in A/4 form
					local res = string.split(Words[4], "/")[1]
					if not res or res == "" then
						res = Words[4]
					end
					hp_sp_mods.HPFactor = tonumber(res)
					hp_sp_mods.SPBase = tonumber(Words[5])
					-- Skip divisor if SPFactor is written in A/4 form
					res = string.split(Words[6], "/")[1]
					if not res or res == "" then
						res = Words[6]
					end
					hp_sp_mods.SPFactor = tonumber(res)
					RaceHPSP[race_id] = hp_sp_mods
				else
					Log(Merge.Log.Warning, "RaceHPSP.txt line %d first field is not a number. Ignoring line.", linenum)
				end
			end
		end
	end

	io.close(TxtTable)
	Game.RaceHPSP = RaceHPSP
end

local function SetRaceHPSPHooks()
	local RaceHPSP = Game.RaceHPSP

	local function GetPlayer(ptr)
		local PlayerId = (ptr - Party.PlayersArray["?ptr"])/Party.PlayersArray[0]["?size"]
		return Party.PlayersArray[PlayerId], PlayerId
	end

	-- GetFullHP HPBase
	autohook(0x48D9C9, function(d)
		local player = GetPlayer(d.esi)
		local race = GetCharRace(player)
		if RaceHPSP and RaceHPSP[race] and RaceHPSP[race].HPBase then
			d.edi = max(d.edi + RaceHPSP[race].HPBase, 1)
		end
	end)

	-- GetFullHP HPFactor
	--  Race HPFactor modifier contains amount of 0.25s so other factors are
	--  multiplied by 4 first and final multiplication is divided by 4 later
	local NewCode = asmpatch(0x48DA05, [[
	sal eax, 2
	nop
	nop
	nop
	nop
	nop
	imul ebx, eax
	sar ebx, 2
	add edi, ebx
	]])

	hook(NewCode + 3, function(d)
		local player = GetPlayer(d.esi)
		local race = GetCharRace(player)
		if RaceHPSP and RaceHPSP[race] and RaceHPSP[race].HPFactor then
			d.eax = max(d.eax + RaceHPSP[race].HPFactor, 0)
		end
	end)

	-- GetFullSP SPFactor
	--  Race SPFactor modifier contains amount of 0.25s so other factors are
	--  multiplied by 4 first and final multiplication is divided by 4 later
	NewCode = asmpatch(0x48DA67, [[
	sal eax, 2
	nop
	nop
	nop
	nop
	nop
	mov ecx, edi
	imul esi, eax
	sar esi, 2
	]])

	hook(NewCode + 3, function(d)
		local player = GetPlayer(d.edi)
		local race = GetCharRace(player)
		if RaceHPSP and RaceHPSP[race] and RaceHPSP[race].SPFactor then
			d.eax = max(d.eax + RaceHPSP[race].SPFactor, 0)
		end
	end)

	-- GetFullSP SPBase
	autohook(0x48DA6C, function(d)
		local player = GetPlayer(d.edi)
		local race = GetCharRace(player)
		if RaceHPSP and RaceHPSP[race] and RaceHPSP[race].SPBase then
			d.esi = max(d.esi + RaceHPSP[race].SPBase, 0)
		end
	end)
end

function events.GameInitialized2()
	ProcessRaceHPSPTxt()
	SetRaceHPSPHooks()
end

Log(Merge.Log.Info, "Init finished: %s", Id)
